import os, datetime
import re
import mistune

markdown = mistune.create_markdown()
CONTENT_PATH = "content/"

class Post():

    STRFTIME = "%d.%m.%Y - %H:%M:%S"

    def __init__(
            self,
            header="",
            title="",
            content="",
            body="",
            id="",
            mod_date_in_s="",
            create_date_in_s="",
        ):
        self.header = header
        self.title = title
        self.content = content
        self.body = body
        self.id = id
        self.mod_date_in_s = mod_date_in_s
        self.create_date_in_s = create_date_in_s

    def get_date_string(self, format=STRFTIME):
        """"
        Parse date in seconds to a string
        """
        return datetime.datetime.fromtimestamp(self.mod_date_in_s).strftime(format)

    def get_plain_header(self):
        """
        Return a plane title without markup
        """
        return re.search(">(.*)<", self.header).group(1)

def generate_posts(content_path=CONTENT_PATH):

    posts = []

    for post in os.listdir(content_path):
        file_path = os.path.join(content_path, post)

        with open(file_path, "r") as f:
            lines = f.readlines()
            header = mistune.html(lines[0])
            title = lines[0]
            content = mistune.html("\n".join(lines[1:]))
            body = "\n".join(lines[1:])

        id = post.removesuffix(".md")

        posts.append(Post(
            header=header,
            title=title,
            content=content,
            body=body,
            id=id,
            mod_date_in_s=os.path.getmtime(file_path),
            create_date_in_s=os.path.getctime(file_path),
        ))

    # sort by date, newest first
    posts.sort(key=lambda p: p.mod_date_in_s, reverse=True)

    return posts