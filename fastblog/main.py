import fastblog.post as post
import uvicorn
from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
templates = Jinja2Templates(directory="templates")

@app.get("/", response_class=HTMLResponse)
async def show_posts(request: Request):
    post_list = post.generate_posts()

    return templates.TemplateResponse("md.html", {"request": request, "posts": post_list})

@app.get("/list")
async def get_list(request: Request):
    return post.generate_posts()

def start():
    """Launched with `poetry run start` at root level"""
    uvicorn.run("fastblog.main:app", host="0.0.0.0", port=8000, reload=True)

def deploy():
    """Launched with `poetry run start` at root level"""
    uvicorn.run("fastblog.main:app", host="0.0.0.0", port=8000, proxy_headers=True)